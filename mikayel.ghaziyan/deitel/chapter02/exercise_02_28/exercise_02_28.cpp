/// Mikayel Ghaziyan
/// 16/10/2017
/// Exercise 2.28

#include <iostream>

/// Beginning of the main
int
main()
{
    /// Declaring variables
    int fiveDigitNum;

    /// Obtaining the number
    std::cout << "Please enter a five digit number: ";
    std::cin >> fiveDigitNum;

    /// Checking if the number has more or less than five digits
    if (fiveDigitNum < 10000) {
        std::cout << "Error 1. Must have five digits. Try again " << std::endl;

	return 1;
    }
    if (fiveDigitNum > 99999) {
        std::cout << "Error 1. Must have five digits. Try again " << std::endl;

	return 1;
    }

    /// Separating the digits
    int number1 = fiveDigitNum / 10000;
    int number2 = (fiveDigitNum / 1000) % 10;
    int number3 = (fiveDigitNum / 100) % 10;
    int number4 = (fiveDigitNum / 10) % 10;
    int number5 = fiveDigitNum % 10;

    /// Displaying the result
    std::cout << "The numbers are " 
              << number1 << "   " 
              << number2 << "   " 
              << number3 << "   "
              << number4 << "   " 
              << number5 << "   " 
              << std::endl;

    return 0;
} /// End of the main

/// End of the file

