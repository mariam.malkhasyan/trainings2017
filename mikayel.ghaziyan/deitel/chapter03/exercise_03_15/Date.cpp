#include "Date.hpp"
#include <iostream>
#include <string>

Date::Date (int month, int day, int year)
{
    setMonth(month);
    setDay(day);
    setYear(year);
}

void 
Date::setMonth(int month)
{   
    if (month > 12) {
	std::cout << "Info 1: Invalid value for the month. Must be between 1 and 12" << std::endl;
	dateMonth_ = 1;
	return;	
    }   

    if (month < 0) {
	std::cout << "Info 2: Invalid value for the month. Must be between 1 and 12" << std::endl;
	dateMonth_ = 1;
	return;
    }

    dateMonth_ = month; 
}

void
Date::setDay(int day)
{
    dateDay_ = day;
}

void
Date::setYear(int year)
{
    dateYear_ = year;
}

int
Date::getMonth()
{
    return  dateMonth_ ;
}

int
Date::getDay()
{
    return dateDay_;
}

int
Date::getYear()
{
    return dateYear_;
}

void
Date::displayDate()
{
    std::cout << getMonth() << "/" << getDay() << "/" << getYear() << std::endl; 
}
