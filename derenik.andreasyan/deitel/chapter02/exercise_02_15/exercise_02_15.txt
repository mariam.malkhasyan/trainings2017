a. x = 7 + 3 * 6 / 2 - 1 ;
   
    1. 3*6
    2. 3*6/2
    3. 7+3*6/2
    4. 7+3*6/2-1
       x=15

b. x = 2 % 2 + 2 * 2 - 2 / 2 ; 
    
    1. 2%2
    2. 2*2
    3. 2/2
    4. 2%2+2*2
    5. 2%2+2*2-2/2
       x=3

c. x = ( 3 * 9 * ( 3 + ( 9 * 3 / ( 3 ) ) ) );

    1. ( 3 )
    2. 9 * 3
    3. 9 * 3 / ( 3 )
    4. ( 3 + ( 9 * 3 / ( 3 ) ) )
    5. 3 * 9
    6. 3 * 9 * ( 3 + ( 9 * 3 / ( 3 ) ) )
       x=324  
