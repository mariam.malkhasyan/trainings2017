#include<iostream>
#include<iomanip>

int
main()
{
        int counter = 0, total = 0;
        while (true) {
            int integer;
            std::cout << "Enter the integer: ";
            std::cin >> integer;
            if (9999 == integer) {
                break;    
            }
            total += integer;
            ++counter;
        }
        if (0 == counter) {
            std::cout << "Error 1:The first number not should be 9999" << std::endl;
            return 1;
        }
        double average = static_cast<double>(total) / counter;
        std::cout << "average is: " << std::setprecision(2) << std::fixed << average << std::endl;
        return 0;
}
