#include<iostream>

int
main()
{
    int amount, smallest = 2147483647;
    std::cout << "Input the amount: ";
    std::cin >> amount;
 
    for (int i = 1; i <= amount; ++i) {
        int number;
        std::cout << "\nEntered the number: ";
        std::cin >> number;
        if (smallest >= number) {
            smallest = number;
        }
    }
    std::cout << "\nsmallest is " << smallest << std::endl;
    return 0;
}
