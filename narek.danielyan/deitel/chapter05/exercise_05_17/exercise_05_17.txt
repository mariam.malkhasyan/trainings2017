in allof these case the brackets are need except g and. 
a) int i = 1, k = 3, j = 2, m = 2;
   std::cout << (i == 1) << std::endl;
   it will type 1
   the brackets are need;
b)
   std::cout << (j == 3) << std::endl;
   it will type 0
   the brackets are need;
c)
   std::cout << (i >= 1 && j < 4) << std::endl;
   it will type 1
   the brackets are need;
d)
   std::cout << (m <= 99 && k < m) << std::endl;
   it will type 0
   the brackets are need
e)
   std::cout << (j >= i || k == m) << std::endl;
   it will type 1
   the brackets are need
f)
   std::cout << (k + m < j || 3 - j >= k) << std::endl;
   it will type 0
   the brackets are need;
g)
  std::cout << (!m) << std::endl;
  it will type 0
  do not put the brackets
h)
 std::cout << (!(j - m)) << std::endl;
 it will type 1
  do not put the first brackets 
i)
 std::cout << (!(r > m)) << std::endl;
   the programm won't compile because r isn't declared
