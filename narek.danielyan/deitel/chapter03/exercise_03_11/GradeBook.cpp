#include "GradeBook.hpp"
#include <iostream>
#include <string>

GradeBook::GradeBook(std::string nameOfCourse, std::string nameOfTeacher)
{
    setCourseName(nameOfCourse);
    setCourseTeacher(nameOfTeacher);
}

void 
GradeBook::setCourseName(std::string nameOfCourse) 
{
    courseName_ = nameOfCourse;
}

void 
GradeBook::setCourseTeacher(std::string nameOfTeacher)
{
    courseTeacher_ = nameOfTeacher;
}

std::string 
GradeBook::getCourseName()
{ 
    return courseName_;
}

std::string 
GradeBook::getCourseTeacher()
{
    return courseTeacher_;
}

void 
GradeBook::displayMessage()
{
    std::cout << "Welcome to the grade book for\n" << getCourseName()
              << "!" << std::endl;
    std::cout << "This course is presented by: " << getCourseTeacher()
              << std::endl;
}
    
