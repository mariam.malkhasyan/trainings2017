class Date
{
public:
    Date(int Month, int day, int year);
    void setMonth(int month);
    void setDay(int day);
    void setYear(int year);
    int getMonth();
    int getDay();
    int getYear();
    void displayDate();
private:
    int month_, day_, year_;
};
