#include <iostream>

int
main()
{
    int number1;
    int number2;
    std::cout << "Enter first number: ";
    std::cin >> number1;
    std::cout << "Enter second number: ";
    std::cin  >> number2;

    std::cout << "sum: " << (number1 + number2) << std::endl;
    std::cout << "product: " << (number1 * number2) << std::endl;
    std::cout << "difference: " << (number1 - number2) << std::endl;

    if (0 == number2) {
        std::cout << "Error 1: Division by 0.\n" << std::endl;
        return 1;
    }

    std::cout << "quotient: " << (number1 / number2) << std::endl;
    return 0;
}

