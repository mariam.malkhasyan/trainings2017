#include <iostream>
#include <climits>

int
main()
{
    int quantity;

    std::cout << "Please enter the quantity of your numbers: ";
    std::cin >> quantity;

    if (quantity < 1) {
        std::cerr << "Error 1: Quantity can't be zero or negative." << std::endl;
        return 1;
    }

    int smallest = INT_MAX;
    
    for (int counter = 1; counter <= quantity; ++counter) {
        int number;
        
        std::cout << "Enter a number (" << counter << " of " << quantity << ")";
        std::cin >> number;

        if (number < smallest) {
            smallest = number;
        }
    }

    std::cout << "The smallest number: " << smallest << std::endl;

    return 0;
}
