Program asks user to input x and y integers in range 1-20.
Result of program is

        x
    @@@@@...@
    @@@@@...@
  y @@@@@...@
       ...
    @@@@@...@

For example. if x is 3 and y is 5. Result will be

@@@
@@@
@@@
@@@
@@@

