#include <iostream>

int
reverse(int number)
{
    int reverse = 0;
    
    while (number > 0) {
        reverse = reverse * 10 + number % 10;
        number /= 10;
    }

    return reverse;
}

int
main()
{
    std::cout << "48923 reverse is " << reverse(48923) << std::endl; 

    return 0;
}
