#include <iostream>
#include <assert.h>

bool 
even(const int number)
{
    assert(number != 0);
    return 0 == number % 2;
}

int
main()
{
    while (true) {
        int number;

        std::cout << "Please enter a number(-1 to exit): ";
        std::cin >> number;

        if (0 == number) {
            std::cout << "Error 1: Number can't be zero. Zero not a positive/negative number" << std::endl;
            return 1;
        }
        if (-1 == number) {
            break;
        }
        if (even(number)) {
            std::cout << number << " is even" << std::endl;
        } else {
            std::cout << number << " is odd" << std::endl;
        }
    }

    return 0;
}
