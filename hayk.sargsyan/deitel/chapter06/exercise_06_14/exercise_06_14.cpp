#include <iostream>
#include <cmath>

double
roundToInteger(const double number)
{
    double calculation = std::floor(number * 10 + 0.5) / 10;

    return calculation;
}

double
roundToTenths(const double number)
{
    double calculation = std::floor(number * 100 + 0.5) / 100;
    
    return calculation;
}

double
roundToHundreths(const double number)
{
    double calculation = std::floor(number * 1000 + 0.5) / 1000;

    return calculation;
}

double
roundToTousanths(const double number)
{
    double calculation = std::floor(number * 10000 + 0.5) / 10000;

    return calculation;
}

int
main()
{
    std::cout << "Initial number: 10.1234\tRounded Number: " << roundToInteger(10.1234) << std::endl;
    std::cout << "Initial number: 10.1234\tRounded Number: " << roundToTenths(10.1234) << std::endl;
    std::cout << "Initial number: 10.1234\tRounded Number: " << roundToHundreths(10.1234) << std::endl;
    std::cout << "Initial number: 10.1234\tRounded Number: " << roundToTousanths(10.1234) << std::endl;

    return 0;
}
