#include <iostream>
#include <cmath>
#include <assert.h>

void
rhombus(const int side, const char fillCharacter)
{
    assert(side > 0);
    
    for (int counterY = -side / 2; counterY <= side / 2; ++counterY) {
        for (int counterX = -side / 2; counterX <= side / 2; ++counterX) {
            std::cout << ((std::abs(counterX) + std::abs(counterY) <= side / 2) ? fillCharacter : ' '); 
        }

        std::cout << std::endl;
    }
}

void
solidRectangle(const int side, const char fillCharacter) 
{
    assert(side > 0);
    
    for (int counterY = 1; counterY <= side; ++counterY) {
        for (int counterX = 1; counterX <= side; ++counterX) {
            std::cout << fillCharacter;
        }

        std::cout << std::endl;
    }
}

void
spacedRectangle(const int side, const char fillCharacter) 
{
    assert(side > 0);
    
    for (int counterY = 1; counterY <= side; ++counterY) {
        for (int counterX = 1; counterX <= side; ++counterX) {
            if (1 == counterX || 1 == counterY || side == counterY || side == counterX) {
                std::cout << fillCharacter;
            } else {
                std::cout << " ";
            }
        }

        std::cout << std::endl;
    }
}

void
triangle(const int side, const char fillCharacter) 
{
    assert(side > 0);
    
    for (int counterY = 1; counterY <= side; ++counterY) {
        for (int counterX = 1; counterX <= side; ++counterX) {
            if (counterX <= counterY) {
                std::cout << fillCharacter;
            }
        }

        std::cout << std::endl;
    }
}

void
drawer(const int side, const char fillCharacter, const int shape) 
{
    assert(side > 0);
    assert(shape > 0 && shape < 5);

    if (1 == shape) {
        triangle(side, fillCharacter);
    } else if (2 == shape) {
        solidRectangle(side, fillCharacter); 
    } else if (3 == shape) {
        rhombus(side, fillCharacter);
    } else if (4 == shape) {
        spacedRectangle(side, fillCharacter);
    }
}

int
main()
{
    int side;

    std::cout << "Please enter the side: ";
    std::cin >> side;

    if (side < 1) {
        std::cerr << "Error 1: Side can't be zero or negative" << std::endl;
        return 1;
    }

    char fillCharacter;

    std::cout << "Please enter the fill character: ";
    std::cin >> fillCharacter;

    int shape;

    std::cout << "Please choose your number of shape: " << std::endl
              << "1 - triangle" << std::endl
              << "2 - solid rectangle" << std::endl
              << "3 - rhombus" << std::endl
              << "4 - spaced rectangle" << std::endl;

    std::cin >> shape;

    if (shape < 1 || shape > 4) {
        std::cerr << "Error 2: There is no such shape" << std::endl;
        return 2;
    }

    drawer(side, fillCharacter, shape);

    return 0;
}
