#include <iostream>

template <typename T>

T
max(const T variable1, const T variable2)
{
    return variable1 > variable2 ? variable1 : variable2;
}

int
main()
{
    std::cout << "The maximal of 3 and 4 is " << max(3, 4) << std::endl;
    std::cout << "The maximal of 3.457 and 0.1 is " << max(3.457, 0.1) << std::endl;
    std::cout << "The maximal of 'a' and 'b' is " << max('a', 'b') << std::endl;
 
    return 0;
}
