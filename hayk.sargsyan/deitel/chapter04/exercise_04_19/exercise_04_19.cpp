#include <iostream>
#include <climits>

int
main()
{ 
    int counter = 1;
    int largest = INT_MIN;
    int secondLargest = INT_MIN;
    
    while (counter <= 10) {
        int number;
        std::cout << "Enter a value (" << counter << " of 10): ";
        std::cin >> number;

        if (number > largest) {
            secondLargest = largest;
            largest = number;
        } else if(number > secondLargest) {
            secondLargest = number;
        }
        
        ++counter;
    }

    std::cout << "The largest value: " << largest << std::endl;
    std::cout << "The second largest value: " << secondLargest << std::endl;

    return 0;
}
