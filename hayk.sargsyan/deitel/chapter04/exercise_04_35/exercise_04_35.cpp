#include <iostream>
#include <cmath>

int
main()
{
    { ///a)
        
        int number;
        
        std::cout << "Please enter positive integer: ";
        std::cin >> number;
        
        int counter = 1;
        int factorial = 1;

        while (counter <= number) {
            factorial *= counter;
            ++counter;
        }

        std::cout << "Factorial of " << number << " is " << factorial << std::endl;
    
    }


    { ///b)

        int accuracy;

        std::cout << "Please enter accuracy of e: ";
        std::cin >> accuracy;
        
        float eFactorial = 1;
        float factorial = 1;
        int counter = 1;
        
        while (counter <= accuracy) {
            factorial *= counter;
            eFactorial += 1 / factorial;
            ++counter;
        }

        std::cout << "e is " << eFactorial << std::endl;
    
    }


    { ///c)
        
        int exponent;
        
        std::cout << "Please enter exponent of e: ";
        std::cin >> exponent;
        
        int accuracy;

        std::cout << "Please enter accuracy of e^x: ";
        std::cin >> accuracy;

        float eFactorial = 1;
        float factorial = 1;
        int counter = 1;
        int multiple = 1;
        
        while (counter <= accuracy) {
            factorial *= counter;
            multiple *= exponent;
            eFactorial += multiple / factorial;
            ++counter;
        }

        std::cout << "e is " << eFactorial << std::endl;
    
    }

    return 0;
}
