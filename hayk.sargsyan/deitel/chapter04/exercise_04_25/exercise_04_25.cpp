#include <iostream>

int
main()
{
    int rectangleSide;
    
    std::cout << "Please enter side of your rectangle: ";
    std::cin >> rectangleSide;

    if (rectangleSide <= 0) {
        std::cerr << "Error 1: rectangle side must be positive integer value" << std::endl;
        return 1;
    }
    
    int counterX = 1;

    while (counterX <= rectangleSide) {
        int counterY = 1;
        while (counterY <= rectangleSide) {
            if (1 == counterX) {
                std::cout << "*";
            } else if (rectangleSide == counterX) {
                std::cout << "*"; 
            } else if (1 == counterY) {
                std::cout << "*";
            } else if (rectangleSide == counterY) {
                std::cout << "*";
            } else {
                 std::cout << " "; 
            }
            
            ++counterY;
        }

        std::cout << std::endl;
        
        counterY = 1;
        ++counterX;     
    }

    return 0;
}
