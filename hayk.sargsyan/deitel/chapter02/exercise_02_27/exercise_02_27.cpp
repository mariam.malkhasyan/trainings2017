#include <iostream>

int
main()
{
    std::cout << "A - " << static_cast<int>('A') << "\n" 
              << "B - " << static_cast<int>('B') << "\n"
              << "C - " << static_cast<int>('C') << "\n"
              << "a - " << static_cast<int>('a') << "\n"
              << "b - " << static_cast<int>('b') << "\n"
              << "c - " << static_cast<int>('c') << "\n"
              << "0 - " << static_cast<int>('0') << "\n"
              << "1 - " << static_cast<int>('1') << "\n"
              << "2 - " << static_cast<int>('2') << "\n"
              << "$ - " << static_cast<int>('$') << "\n"
              << "* - " << static_cast<int>('*') << "\n"
              << "+ - " << static_cast<int>('+') << "\n" 
              << "/ - " << static_cast<int>('/') << "\n"
              << "space - " << static_cast<int>(' ') << "\n";

    return 0;
}

