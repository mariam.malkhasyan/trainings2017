    Source-code file is  .cpp file, which contains a class definition and a main function. When building an object-oriented C ++ program , it is customary to definere usable source-code (such as a class) in a file that by convention has a .h file name extension. 
   .h file is called header file. Programs use  #include preprocessor directives to include header files and take advantage of reusable software components, such as type string provided in the C ++ Standard Library and user-defined types.

