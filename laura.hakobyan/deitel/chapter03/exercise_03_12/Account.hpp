#include <iostream>

class Account
{
public:
    Account(int balance);
    void setBalance(int balance);
    void credit(int creditAmount);
    void debit(int debitAmount);
    int getBalance();
    void displayMessage();
private:
    int balance_;
};

