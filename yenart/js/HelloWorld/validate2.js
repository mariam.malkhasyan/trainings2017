"use strict";
function myFunction() {
    var x = document.getElementById("numb").value; /// Get the value of the input
    if (isNaN(x) || x < 1 || x > 10) { /// If not Number or less than 1 or greater than 10
        document.getElementById("demo").innerHTML = "Input not valid";
    } else {
        document.getElementById("demo").innerHTML = "Input OK";
    }
    console.log("Done.");
}

