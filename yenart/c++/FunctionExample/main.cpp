#include <iostream>
#include <fstream>

#include <unistd.h>

char
getRegister(std::ifstream&     fin,
            const bool         isInteractive,
            const std::string& prompt,
            const bool         isFileInput);

int
main(int argc, char** argv)
{
    std::ifstream fin((argc > 1) ? argv[1] : "");
    const bool isInt = ::isatty(STDIN_FILENO);
    const bool isFile = fin.is_open();
    
    const char register1Number = getRegister(fin, isInt, "Register 1: ", isFile);
    const char register2Number = getRegister(fin, isInt, "Register 2: ", isFile);
    const char register3Number = getRegister(fin, isInt, "Register 3: ", isFile);

    std::cout << register1Number << register2Number << register3Number << std::endl;
    return 0;
}

char
getRegister(std::ifstream&     fin,
            const bool         isInteractive,
            const std::string& prompt,
            const bool         isFileInput)
{
    char reg;
    if (isInteractive && !isFileInput) {
        std::cout << prompt;
    }
    if (isFileInput) { /// fin.is_open()
        fin >> reg;
    } else {
        std::cin >> reg;
    }
    return reg;
}

