#include <iostream>

int
main()
{
    int number1;
    int number2;
    
    std::cout << "Enter two numbers: ";
    std::cin  >> number1 >> number2;
    
    if (0 == number2) {
        std::cout << "Second number should not be 0, as a number cannot be divided by 0"<< std::endl;
        return 1;
    }

    if (0 == number1 % number2) {
        std::cout << number1 << " is a multiple of " << number2 << std::endl;
        return 0;
    }

    std::cout << number1 << " is not a multiple of " << number2 << std::endl;
    
    return 0;
}

