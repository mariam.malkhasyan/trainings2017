#include <iostream>

int
main()
{
    int number;
    
    std::cout << "Enter a number: ";
    std::cin >> number;
    
    if (0 == number % 2) {
        std::cout << number << " is an even number" << std::endl;
        return 0;
    }

    std::cout << number << " is an odd number" << std::endl;
   
    return 0;
}
