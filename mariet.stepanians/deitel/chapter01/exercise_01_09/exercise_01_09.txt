a. Structured programming is easy to understand and it’s a raw material in order to           learn OOP.

b. Definition of the context of the system, designing system architecture, identification of the objects in the system, construction of design models, specification of object interfaces

c. Almost everyday we send messages to each other. But for an example i’ll take the case when the judge announce the audience that the defendant is not guilty. 

d. Car radio detects specific channels and presents the content and permits the driver/passenger to change the volume and channels with appropriate controllers.
