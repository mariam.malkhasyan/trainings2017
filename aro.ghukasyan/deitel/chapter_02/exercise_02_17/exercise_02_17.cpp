#include <iostream>

int
main()
{
    std::cout << "1 2 3 4\n" ;  ///version a

    std::cout << "1 " << "2 " << "3 " << "4 " <<std::endl;  ///version b 

    std::cout << "1 ";  ///version c
    std::cout << "2 ";
    std::cout << "3 ";
    std::cout << "4\n";

    return 0;
}
