a) false, some operations in C++ are done from right to left, for example assignment operation.
b) true, they are all valid, names should start by a letter or underline, but shouldn't start by a number.
c) false, in std::cout << "a = 5 "; statement in the parentheses is just a text and it is just outputted.
d) false, operations have their precedence. If there are no parentheses operations * / and % should be done at first and only after that + and - are used.
e) false, they are all invalid, except h22, which starts with letter.

