#include <iostream>

int
main()
{
    int number;

    std::cout << "Enter a five-digit number: ";
    std::cin >> number;

    if (number <= 9999) {
        std::cout << "Error 1: the entered number should be five-digit." << std::endl;
        return 1;
    }
    if (number >= 100000) {
        std::cout << "Error 1: the entered number should be five-digit." << std::endl;
        return 1;
    }

    int a = number % 10;
    int b = number / 10 % 10;
    int c = number / 100 % 10;
    int d = number / 1000 % 10;
    int e = number / 10000;

    std::cout << e << "   " << d << "   " << c << "   " << b << "   " << a << std::endl;

    return 0;
}

