#include <iostream>

int
main()
{
    int number;

    std::cout << "Enter number: ";
    std::cin >> number;

    if (0 == number % 2) {
        std::cout << "Entered number is even." << std::endl;
    }

    if (1 == number % 2) {
        std::cout << "Entered number is odd." << std::endl;
    }

    return 0;
}

