#include <iostream>

int
main()
{
    std::cout << "Pythagorean Triples:" << std::endl;
    for (int side1 = 1; side1 <= 500; ++side1) {
        int squareSide1 = side1 * side1;
        for (int side2 = side1 + 1; side2 <= 500; ++side2) {
            int sumSide = side1 + side2;
            int squareSide2 = side2 * side2;
            int squareSideSum = squareSide1 + squareSide2;
            for (int hypotenuse = side2 + 1; hypotenuse <= 500 && hypotenuse < sumSide; ++hypotenuse) {
                int squareHypotenuse = hypotenuse * hypotenuse;
                if (squareSideSum == squareHypotenuse) {
                    std::cout << side1 << ", " << side2 << ", " << hypotenuse << std::endl;
                }
            }
        }
    }
    return 0;
}

