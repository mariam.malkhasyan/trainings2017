#include <iostream>

int
main()
{
    int count;
    for (count = 1; count <= 10; ++count) {
        if (5 != count) {
            std::cout << count << " ";
        }
    }
    std::cout << "\nSkiped printing 5" << std::endl;
    return 0;
}

