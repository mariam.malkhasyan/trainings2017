#include <iostream>
#include <iomanip>

int
main()
{
    for (int row = 1; row <= 10; ++row) {
        for (int column = 1; column <= row; ++column) {
            std::cout << '*';
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    for (int row =1; row <= 10; ++row) {
        for (int column = 10; column >= row; --column) {
            std::cout << '*';
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    for (int row = 1; row <= 10; ++row) {
        std::cout << std::setw(row);
        for (int column = row; column <= 10; ++column) {
            std::cout << '*';
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    for (int row = 10; row >= 1; --row) {
        std::cout << std::setw(row);
        for (int column = row; column <= 10; ++column) {
            std::cout << '*';
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    return 0;
}

