#include <iostream>

int
main()
{
    int factorial = 1;
    std::cout << "Number \t Factorial" << std::endl;
    
    for (int i = 1; i <= 5; ++i) {
        factorial *= i;
        std::cout << i << "\t " << factorial << std::endl;
    }

    return 0;
}

