#include <iostream>

int
main()
{
    int number = 2;
    while (true) {
        number *= 2;
        std::cout << number << std::endl;
        /// after printing first 30 degrees of 2, it multiplies 2^30 by 2
        /// we get 2^31 which is out of integers maximal range
        /// 2^31 binary equivalent is 10000000 00000000 00000000 00000000
        /// INT_MAX is 2147483647 or 01111111 11111111 11111111 11111111
        /// computer uses "two's complement" notation, which means, that
        /// negative number is represented from positive by taking ones'
        /// complement and then adding one
        /// for positive numbers highest bit (the left one) is 0
        /// for negative ones it is 1
        /// so 10000000 00000000 00000000 00000000 is negative for computer
        /// lets find out which numbers negative it is
        /// 1) 10000000 00000000 00000000 00000000 (as negative number)
        /// 2) 01111111 11111111 11111111 11111111 (taking compliment)
        /// 3) 10000000 00000000 00000000 00000000 (adding one)
        /// so 2^31 is represented as -2^31 (-2147483648) and program prints that
        /// after multiplying it again by 2 we get 1 00000000 00000000 00000000 00000000
        /// highest bit is thrown away, because we reach the given memory for integer
        /// we get 0, which is multiplied by 2 and printed infinite times
    }
    return 0;
}

