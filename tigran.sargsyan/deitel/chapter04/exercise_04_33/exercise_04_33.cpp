#include <iostream>

int
main()
{
    int side1, side2, side3;
    std::cout << "Enter triangles sides: ";
    std::cin >> side1 >> side2 >> side3;

    if (side1 <= 0) {
        std::cerr << "Error 1. The triangle sides should be positive." << std::endl;
        return 1;
    }
    if (side2 <= 0) {
        std::cerr << "Error 1. The triangle sides should be positive." << std::endl;
        return 1;
    }
    if (side3 <= 0) {
        std::cerr << "Error 1. The triangle sides should be positive." << std::endl;
        return 1;
    }

    int side1Square = side1 * side1;
    int side2Square = side2 * side2;
    int side3Square = side3 * side3;
    if (side1Square != side2Square + side3Square) {
        if (side2Square != side1Square + side3Square) {
            if (side3Square != side1Square + side2Square) {
                std::cout << "Sides doesn't represent right triangle." << std::endl;
                return 0;
            }
        }
    }
    std::cout << "Sides represent right triangle." << std::endl;
    return 0;
}

