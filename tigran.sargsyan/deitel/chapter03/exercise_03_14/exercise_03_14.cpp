#include "Employee.hpp"

#include <iostream>
#include <string>

int
main()
{
    Employee employee1("Donald", "Trump", 500000);
    Employee employee2("George", "Bush", 400000);

    int yearlySalary1 = 12 * employee1.getSalary();
    std::cout << "Employee 1:\nName:" << employee1.getName()
        << "\nSurname: " << employee1.getSurname()
        << "\nSalary: " << employee1.getSalary()
        << "\nYearly salary: " << yearlySalary1 << std::endl;
    int yearlySalary2 = 12 * employee2.getSalary();
    std::cout << "Employee 2:\nName:" << employee2.getName()
        << "\nSurname: " << employee2.getSurname()
        << "\nSalary: " << employee2.getSalary()
        << "\nYearly salary: " << yearlySalary2 << std::endl;

    int salary1 = 1.1 * employee1.getSalary();
    employee1.setSalary(salary1);
    int salary2 = 1.1 * employee2.getSalary();
    employee2.setSalary(salary2);

    int newYearlySalary1 = 12 * employee1.getSalary();
    std::cout << "Employee 1:\nName:" << employee1.getName()
        << "\nSurname: " << employee1.getSurname()
        << "\nSalary: " << employee1.getSalary()
        << "\nYearly salary: " << newYearlySalary1 << std::endl;
    int newYearlySalary2 = 12 * employee2.getSalary();
    std::cout << "Employee 2:\nName:" << employee2.getName()
        << "\nSurname: " << employee2.getSurname()
        << "\nSalary: " << employee2.getSalary()
        << "\nYearly salary: " << newYearlySalary2 << std::endl;

    Employee employee3("", "", 0);

    std::string name;
    std::cout << "Enter name: ";
    getline(std::cin, name);
    employee3.setName(name);

    std::string surname;
    std::cout << "Enter surname: ";
    getline(std::cin, surname);
    employee3.setSurname(surname);

    int salary;
    std::cout << "Enter salary: ";
    std::cin >> salary;
    employee3.setSalary(salary);

    std::cout << "Employee 3:\nName:" << employee3.getName()
        << "\nSurname: " << employee3.getSurname() 
        << "\nSalary: " << employee3.getSalary() << std::endl;

    return 0;
}

