#include <iostream>

int
main()
{
    char subParagraph;
    std::cout << "Enter sub paragraph a, b, c or d: ";
    std::cin  >> subParagraph;

    switch (subParagraph) {
    case 'a': {
        std::cout << "Case 'a' is !(х < 5) && !(y >= 7).\n" << std::endl;
        std::cout << "Case are equivalent in (x >= 5) && (y < 7)." << std::endl; 
        int x;
        std::cout << "Enter x: ";
        std::cin  >> x;
        int y;
        std::cout << "Enter y: ";
        std::cin  >> y;
        
        if (((!(x < 5) && !(y >= 7)) == ((x >= 5) && (y < 7)))  && (((x >= 5) && (y < 7)) == true)) {
            std::cout << "Numbers satisfy the condition." << std::endl;
        } else {
            std::cout << "Numbers are not satisfied with condition." << std::endl;
        }
        break;
    }
    case 'b': {
        std::cout << "Case 'b' is !(а == Ь) || !(g != 5).\n" << std::endl;
        std::cout << "Case are equivalent in (a != b) || (5 == g))." << std::endl; 
        int a;
        std::cout << "Enter a: ";
        std::cin  >> a;
        int b;
        std::cout << "Enter b: ";
        std::cin  >> b;
        int g;
        std::cout << "Enter g: ";
        std::cin  >> g;
        
        if ((!(a == b) || !(g != 5)) == ((a != b) || (5 == g)) && (((a != b) || (5 == g)) == true)) {
            std::cout << "Numbers satisfy the condition." << std::endl;
        } else {
            std::cout << "Numbers are not satisfied with condition." << std::endl;
        }
        break;
    }
    case 'c': {
        std::cout << "Case 'c' is !((x <= 8) && (y > 4)).\n" << std::endl;
        std::cout << "Case are equivalent in (x > 8) || (y <= 4)." << std::endl; 
        int x;
        std::cout << "Enter x: ";
        std::cin  >> x;
        int y;
        std::cout << "Enter y: ";
        std::cin  >> y;
        
        if (((!((x <= 8) && (y > 4))) == ((x > 8) || (y <= 4))) && (((x > 8) || (y <= 4)) == true)) {
            std::cout << "Numbers satisfy the condition." << std::endl;
        } else {
            std::cout << "Numbers are not satisfied with condition." << std::endl;
        }
        break;
    }
    case 'd': {
        std::cout << "Case 'd' is !((i > 4) || (j <= 6)).\n" << std::endl;
        std::cout << "Case are equivalent in (i <= 4) && (j > 6)." << std::endl; 
        int i;
        std::cout << "Enter i: ";
        std::cin  >> i;
        int j;
        std::cout << "Enter j: ";
        std::cin  >> j;
        
        if (((!((i > 4 || (j <= 6)))) == ((i <= 4) && (j > 6))) && (((i <= 4) && (j > 6)) == true)) {
            std::cout << "Numbers satisfy the condition." << std::endl;
        } else {
            std::cout << "Numbers are not satisfied with condition." << std::endl;
        }
        break;
    }
    default:
        std::cout << "Empty case." << std::endl;
        break;
    }
    return 0;
}

