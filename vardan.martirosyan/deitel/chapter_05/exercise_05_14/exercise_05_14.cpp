#include <iostream>

int
main()
{
    double total = 0;
    while (true) {
        int productNumber;
        std::cout << "Enter product number (-1 to quit): ";
        std::cin  >> productNumber;
        
        if (-1 == productNumber) {
            break;
        }

        int quantity;
        std::cout << "Enter quantity sold: ";
        std::cin  >> quantity;
        if (quantity <= 0) {
            std::cerr << "Error 1: Quantity can not be negative." << std::endl;
            return 1;
        }
        switch (productNumber) {
        case 1: total += quantity * 2.98; break;
        case 2: total += quantity * 4.50; break;
        case 3: total += quantity * 9.98; break;
        case 4: total += quantity * 4.49; break;
        case 5: total += quantity * 6.87; break;
        default:
            std::cerr << "Error 2: Your product number is invalid." << std::endl;
            return 2;
        }
    }
    std::cout << "Total is " << total << std::endl;
    return 0;
}
