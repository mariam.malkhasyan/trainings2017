#include <iostream>

int
main()
{
    for (int quantity = 1; quantity <= 5; ++quantity) {
        int number;
        std::cout << "Enter number" << quantity << ": ";
        std::cin  >> number;
        if (number < 1 || number > 30) {
            std::cerr << "Error 1: Number must be from 1 to 30." << std::endl;
            return 1;
        }
        for (int simvol = number; simvol > 0; --simvol) {
            std::cout << "*";
        }
        std::cout << std::endl;
    }
    return 0;
}

