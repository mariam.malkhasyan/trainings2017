#include <iostream>
#include <cmath>

int
main()
{   
    for (int yAxis= -4; yAxis <= 4; ++yAxis) {
        for (int xAxis = -4; xAxis <= 4; ++xAxis) {
            std::cout << ((std::abs(xAxis) + std::abs(yAxis) <= 4) ? "*" : " "); 
        }
        std::cout << std::endl;
    }

    return 0;
}

