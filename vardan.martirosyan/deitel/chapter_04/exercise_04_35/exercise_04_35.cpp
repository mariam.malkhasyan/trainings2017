#include <iostream>

int
main()
{
    char subExercise;
    std::cout << "a)\nb)\nc)\nChoose sub-exercise: ";
    std::cin  >> subExercise;

    if ('a' == subExercise) {
        int number;
        std::cout << "Enter number: ";
        std::cin  >> number;
        if (number < 0) {
            std::cerr << "Error 1: Number can not be negative." << std::endl;
            return 1;
        }       
        double factorial = 1;
        while (number > 0) {
            factorial *= number;
            --number;
        }
        std::cout << "Factorial is " << factorial << std::endl;
        return 0;
    }
    
    if ('b' == subExercise) {
        int accuracy;
        std::cout << "Enter accuracy: ";
        std::cin  >> accuracy;
        if (accuracy < 0) {
            std::cerr << "Error 2: Accuracy can not be negative." << std::endl;
            return 2;
        }       

        double factorial = 1, number = 1, e = 1;
        while (number < accuracy) {
            factorial *= number;
            e += 1.0 / factorial;
            ++number;
        }
        std::cout << "e is " << e << std::endl;
        return 0;
    }

    if ('c' == subExercise) {
        int accuracy;
        std::cout << "Enter accuracy: ";
        std::cin  >> accuracy;

        if (accuracy < 0) {
            std::cerr << "Error 2: Accuracy can not be negative." << std::endl;
            return 2;
        }

        int x;
        std::cout << "Enter power: ";
        std::cin  >> x;

        double factorial = 1, number = 1, e = 1, powX = 1;
        while (number <= accuracy) {
            powX *= x;
            factorial *= number;
            ++number;
            e += powX / factorial;
        }
        std::cout << "e^x is " << e << std::endl;
        return 0;
    }
    std::cerr << "Error 3: Incorrect choice." <<std::endl;
    return 3;
}

