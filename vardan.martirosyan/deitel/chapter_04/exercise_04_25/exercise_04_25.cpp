#include <iostream>

int
main()
{
    int side;

    std::cout << "Enter square width: ";
    std::cin  >> side;

    if (side < 1) {
        std::cout << "Error 1: The side should be in the range [1, 20]." << std::endl;
        return 1;
    }
    if (side > 20) {
        std::cout << "Error 1: The side should be in the range [1, 20]." << std::endl;
        return 1;
    }

    int i = 1;
    while (i <= side) {
        int j = 1;
        while (j <= side) {
            if (1 == i) {
                std::cout << "*";
            } else if (i == side) {
                std::cout << "*";         
            } else if (1 == j) { 
                std::cout << "*";
            } else if (j == side) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }
            ++j;
        }
        std::cout << std::endl;
        ++i;
    }
    std::cout << std::endl;
    return 0;
}
