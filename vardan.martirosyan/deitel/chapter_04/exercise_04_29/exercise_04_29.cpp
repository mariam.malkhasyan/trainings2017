#include <iostream>

int
main()
{
    int number = 1;
    while (true) {
        number = number * 2;
        std::cout << number << ", ";
    }
    return 0;
}

