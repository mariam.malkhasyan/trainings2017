A class can provide for a data element a set-function and get-function to protect data from direct access.
Set the value of any data of the class (set ()), get the value of any data of the class (get ()).
