#include <string>

class Invoice
{
public:
    Invoice(int number, std::string description, int quantity, int price);
    void setPartNumber(int number);
    void setPartDescription(std::string description);
    void setItemQuantity(int quantity);
    void setPricePerItem(int price);
    int getPartNumber();
    std::string getPartDescription();
    int getItemQuantity();
    int getPricePerItem();
    int getInvoiceAmount();

private:
    int partNumber_;
    std::string partDescription_;
    int itemQuantity_;
    int pricePerItem_;
};
