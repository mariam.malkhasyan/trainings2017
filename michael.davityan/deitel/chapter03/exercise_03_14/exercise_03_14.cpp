#include "Employee.hpp"
#include <iostream>
#include <string>

int
main()
{
    std::string name;
    std::string surname;
    int salary;
   
    std::cout << "Insert name: ";
    std::cin >> name;
    std::cout << "Insert surname: ";
    std::cin >> surname;
    std::cout << "Insert salary: ";
    std::cin >> salary;
    Employee employee1(name, surname, salary);
    std::cout << "\n"<< employee1.getName() << "\n";
    std::cout << employee1.getSurname() << "\n";
    std::cout << employee1.getSalary() * 12 << "\n";
    salary += (salary * 10) / 100;
    employee1.setSalary(salary);
    
    std::cout << employee1.getName() << "\n";
    std::cout << employee1.getSurname() << "\n";
    std::cout << employee1.getSalary() * 12 << std::endl;
	    
    std::cout << "Insert name: ";
    std::cin >> name;
    std::cout << "Insert surname: ";
    std::cin >> surname;
    std::cout << "Insert salary: ";
    std::cin >> salary;
    Employee employee2(name, surname, salary);
    std::cout << employee2.getName() << "\n";
    std::cout << employee2.getSurname() << "\n";
    std::cout << employee2.getSalary() * 12 << "\n";
    salary += (salary * 10) / 100;
    employee2.setSalary(salary);

    std::cout << employee2.getName() << "\n";
    std::cout << employee2.getSurname() << "\n";
    std::cout << employee2.getSalary() * 12 << std::endl; 

    return 0;
}
