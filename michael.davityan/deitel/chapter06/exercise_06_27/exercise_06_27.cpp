#include <iostream>
#include <iomanip>

double celsius(const double fahrenheit);
double fahrenheit(const double celsius);

int 
main()
{
    std::cout << "Celsius to Fahrenheit table---------------------------------------------------------------------------------------\n";
    const int LINE_LIMIT1 = 15;        
    const int CELSIUS_LIMIT = 100;
    for (int line = 0; line < LINE_LIMIT1; ++line) {
        for (int celsius = line; celsius <= CELSIUS_LIMIT; celsius += LINE_LIMIT1) {
            std::cout << std::setw(3) << celsius << "C - " << std::setw(5)
                      << fahrenheit(static_cast<double>(celsius)) << "F | ";
        }
        std::cout << std::endl;
    }

    std::cout << "\nFahrenheit to Celsius table--------------------------------------------------------------------------------------\n";
    const int LINE_LIMIT2 = 36;
    const int FAHRENHEIT_LIMIT = 212;
    const int INITIAL_VALUE = 32;
    for (int line = 0; line < LINE_LIMIT2; ++line) {
        for (int fahrenheit = INITIAL_VALUE + line; fahrenheit <= FAHRENHEIT_LIMIT; fahrenheit += LINE_LIMIT2) {
            std::cout << std::setw(3) <<fahrenheit << "F -" << std::setw(9)
                      << celsius(static_cast<double>(fahrenheit)) << "C | ";
        }
        std::cout << std::endl;
    }

    return 0;
}

double 
celsius(const double fahrenheit)
{
    return (fahrenheit - 32.0) * 5.0 / 9;
}

double 
fahrenheit(const double celsius)
{
    return 9.0 / 5 * celsius + 32.0;
}
