Question. For each of the following sets of integers, write a single statement that prints a number at random 
from the set:
a. 2, 4, 6, 8, 10.
b. 3, 5, 7, 9, 11.
c. 6, 10, 14, 18, 22.

Answer. 
a. (rand() % 5 + 1) * 2;
b. (rand() % 5 + 1) * 2 + 1;
c. ((rand() % 5 + 1) * 2 + 1) * 2;
 
 
