Question!!!.
Determine whether each of the following is true or false. If false, explain why.
a. To refer to a particular location or element within an array, we specify the name of the array and the value of the particular element.
b. An array declaration reserves space for the array.
c. To indicate that 100 locations should be reserved for integer array p , the programmer writes the declaration
p[ 100 ];
d. A for statement must be used to initialize the elements of a 15-element array to zero.
e. Nested for statements must be used to total the elements of a two-dimensional array.


Answer!!!.
a. wrong!!!. To refer to a particular location or element within an array, we specify the name of the array and 
the index of the particular element.
b. Its true!!!
c. wrong!!!. The programmer writes the declaration int p[ 100 ];
d. wrong!!!. For example,this can be done with this way int a[15] = {0};
e. Its true!!!.
