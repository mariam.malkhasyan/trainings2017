Question!!!.
Fill in the blanks in each of the following:
a. The names of the four elements of array p ( int p[ 4 ] ;) are _________, _________, _________ and __________.
b. Naming an array, stating its type and specifying the number of elements in the array is called ____________ the array.
c. By convention, the first subscript in a two-dimensional array identifies an element's ____________ and 
the second subscript identifies an
element's ____________.
d. An m-by-n array contains ___________ rows, ________ columns and __________elements.
e. The name of the element in row 3 and colum n 5 of array d is ________.

Answer!!!.
a. The names of the four elements of array p ( int p[ 4 ];) are p[0] ,p[1] , p[2] and p[3].
b. Naming an array, stating its type and specifying the number of elements in the array is called declaring 
the array.
c. By convention, the first subscript in a two-dimensional array identifies an element's row and 
the second subscript identifies an element's column.
d. An m-by-n array contains m rows, n columns and m*n elements.
e. The name of the element in row 3 and column 5 of array d is d[3][5].
